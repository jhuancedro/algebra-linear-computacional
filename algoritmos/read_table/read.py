#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 21:26:08 2018

@author: iagorosa
"""

import pandas as pd
import numpy as np

def leitura_sistema_bernardo(matrix_name, b_name):
    
    f = open(matrix_name, "r")
    
    fb = open(b_name, 'r')
    
    
    f.readline()
    f.readline()
    
    fb.readline()
    fb.readline()
    
    M = []
    
    b = []
    
    count = 0
    
    
    while True:
        
        l = f.readline() 
    
        lb = fb.readline()
        if l != '':
#            print(float(lb))
            b.append(float(lb))
            
            M.append([])
            t = l.split(': (')[1]
            t = t.split(')  (')
            for i in range(len(t)):
                k = t[i].split(',')
                count +=1
                
                if i == len(t)-1:
                    k[-1] = k[-1][:-3]
              
                k[0] = int(k[0])
                k[1] = float(k[1])
                k = tuple(k)
                M[-1].append(k)
        else:
            break
        
    f.close()
    fb.close()
        
    dim = len(M)

    m = np.zeros((dim, dim))
    
    for i in range(dim):
        for j in M[i]:
            m[i, j[0]] = j[1]

    return m, b, 480, 48



m, b, _, _ = leitura_sistema_bernardo("matrixPrimal.txt", 'rhsPrimal.txt')
    
    
    
    
    