import numpy as np

zeros = lambda m, n: np.matrix(np.zeros((m, n)))
rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))


def arnoldi(A: np.matrix, v: np.matrix, k):
    """ Ortonormaliza uma matriz no espaco de krylov """
    # FIXME: Nao ta funcionando
    m, n = A.shape
    Q = zeros(m, n)
    H = zeros(n, n)

    Q[:, 0] = v / np.linalg.norm(v)
    for k in range(n):
        u = A * Q[:, k]
        for j in range(k):
            H[j, k] = Q[:, j].T * u
            u = u - H[j, k] * Q[:, j]
        if k + 1 < m:
            H[k + 1, k] = np.linalg.norm(u)

            print(H)
            if H[k + 1, k] < 1E-8:
                break
            Q[:, k + 1] = u / H[k + 1, k]

    return Q, H


np.random.seed(0)
A = rand_mn(3, 3)
v = rand_mn(3, 1)
# A = np.matrix(
#    [[1, 2],
#     [1, 0],
#     [1, 0]])
print('A =\n', A)

Q, H = arnoldi(A, v, 3)
print('Q =\n', Q)
print('H =\n', H)
# Q, R = householder(A)
#
# print('Q =\n', Q)
# print('R =\n', R)
# print('Erro na fatoração: %e' % np.linalg.norm(A-Q*R))
