"""
    Metodos de algebra linear
"""

from copy import copy
import numpy as np

from matriz_bloco import MatrizBloco

'''
    # # # # # # # # # # # # # # # # # # # # # # 
     METODOS PARA RESOLUCAO DE SISTEMAS SIMPLES
    # # # # # # # # # # # # # # # # # # # # # # 
'''

def pivotamentoSimplesInferior(A: np.matrix, ind: list, k: int):
    """ Pivotamento simples para A eliminaçao gaussiana triangular inferior
        no passo de eliminacao k """
    m, m = A.shape
    if k > m-2: return

    # buscar maior pivo
    pivo = int(k + np.argmax(abs(A[k:, k])))

    # trocando linha atual com A linha do pivo
    # nao troca valores ja nulos de A
    ind[k], ind[pivo] = ind[pivo], ind[k]
    # A[k, k:], A[pivo, k:] = A[pivo, k:].copy(), A[k, k:].copy()
    # B[k, : ], B[pivo, : ] = B[pivo, : ].copy(), B[k, : ].copy()
    pass


def eliminacaoGauss(A_: np.matrix, B_: np.matrix, pivot=True):
    """ Torna A matriz A triangular superior, modificando B,
        de forma que o sistema Ax=B nao eh alterado.
        Nao modifica as matrizes originais. """
    A, B = A_.copy(), B_.copy()
    m, n = A.shape
    ind = list(range(m))    # indexacao para linhas

    # para cada linha
    for k in range(n):
        if pivot:   pivotamentoSimplesInferior(A, ind, k)
        # para cada uma das linhas abaixo
        for i in range(k+1, m):
            m_ik = A[ind[i], k] / A[ind[k], k]
            # modificando todas as posicoes da linha
            A[ind[i], k] = 0 # Posicao que deve ser anulada
            # A[i, k] = m_ik; # Caso da decomposicao LU
            A[ind[i], k+1:n] -= m_ik * A[ind[k], k+1:n]
            B[ind[i], :]     -= m_ik * B[ind[k], :]

    return A, B, ind


def solverTriangularSup(A: np.matrix, B: np.matrix, ind: list):
    """ Resolve o sistama Ax=B, onde A eh triangular superior """
    m, n = A.shape[0], B.shape[1]
    x = np.zeros((m, n))

    for i in range(m-1, -1, -1):
        x[i, :] = B[ind[i], :]
        for j in range(m-1, i, -1):
            x[i, :] -= x[j, :]*A[ind[i], j]
        x[i, :] /= A[ind[i], i]

    return x


def solverElimicacaoGauss(A: np.matrix, B: np.matrix, pivot=True):
    A_, B_, ind = eliminacaoGauss(A, B, pivot)
    return solverTriangularSup(A_, B_, ind)


def erroSolucao(A: MatrizBloco, B: MatrizBloco, x: MatrizBloco):
    return np.linalg.norm(A*x-B)

'''
    # # # # # # # # # # # # # # # # # # # # # # 
    METODOS PARA RESOLUCAO DE SISTEMAS EM BLOCO
    # # # # # # # # # # # # # # # # # # # # # # 
'''

def eliminacaoGaussBloco(A_: MatrizBloco, B_: MatrizBloco, pivot=True):
    """ Torna A matriz em bloco A triangular superior, modificando B,
        de forma que o sistema Ax=B nao eh alterado.
        Nao modifica as matrizes originais.
    """
    A, B = copy(A_), copy(B_)
    m, n = A.m, A.n

    # para cada linha
    for k in range(A.m):
        # TODO: pivotamento??
        # para cada uma das linhas abaixo
        for i in range(k+1, A.m):
            # modificando todas as posicoes da linha
            for j in range(k+1, A.n):
                # A[i, j] -= A[i, k] * ((A[k, k] ** -1) * A[k, j])
                A[i, j] -= A[i, k] * solverElimicacaoGauss(A[k, k], A[k, j], pivot)

            for j in range(B.n):
                # B[i, j] -= A[i, k] * ((A[k, k] ** -1) * B[k, j])
                B[i, j] -= A[i, k] * solverElimicacaoGauss(A[k, k], B[k, j], pivot)

            # FIXME: A[i, k] != 0 nao deve afetar no resultado final!
            A[i, k] = np.zeros(A[i, k].shape)  # Posicao que "deve" ser anulada

    return A, B


def solverTriangularSupBloco(A: MatrizBloco, B: MatrizBloco, pivot=True):
    """ Resolve o sistema em bloco Ax=B, onde A eh triangular superior """
    dimX = A.dim[1], B.dim[1]
    x = np.matrix( np.zeros( (sum(dimX[0]), sum(dimX[1])) ) )
    x = MatrizBloco(x, dimX)

    for i in range(A.m-1, -1, -1):
        for k in range(B.n):
            x[i, k] = B[i, k]
            for j in range(A.m-1, i, -1):
                x[i, k] -= A[i, j]*x[j, k]
            # x[i, k] = (A[i, i] **-1) * x[i, k]
            x[i, k] = solverElimicacaoGauss(A[i, i], x[i, k], pivot)

    return x


def solverElimicacaoGaussBloco(A: MatrizBloco, B: MatrizBloco, pivot=True):
    A_, B_ = eliminacaoGaussBloco(A, B, pivot)
    return solverTriangularSupBloco(A_, B_, pivot)

def erroSolucaoBloco(A: MatrizBloco, B: MatrizBloco, x: MatrizBloco):
    return np.linalg.norm((A*x-B).M)