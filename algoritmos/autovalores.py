"""
	Métodos para encontrar autovalores ou autovetores
"""

import numpy as np

from algoritmos.qr import gram_schmidt

zeros = lambda m, n: np.matrix(np.zeros((m, n)))
rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))


def power_iteration(A_: np.matrix, x0=None, it=1000, inv=False):
    """Metodo da potencia"""
    if x0 is None:  x = rand_mn(A_.shape[0], 1)
    else:           x = x0

    for i in range(it):
        if inv: xi = np.linalg.solve(A_, x)
        else:   xi = A * x

        xi /= np.linalg.norm(xi, np.inf)
        if np.linalg.norm(x - xi) < 1E-7:    break
        x = xi

    # se x[0] = 0, fudeu
    l = float(A_[0, :] * x / x[0, 0])
    # print('it=', i)

    return x, l


def qr_iteration(A_: np.matrix, it=100):
    """Iteracao QR"""
    A = A_.copy()
    for i in range(it):
        Q, R = gram_schmidt(A)
        Ai = R * Q
        print('\n A=\n%s \n Q=\n%s' % (Ai, Q))
        if np.linalg.norm(A.diagonal() - Ai.diagonal()) < 1e-10:    break
        A = Ai

    print('it=', i)
    return A, Q


def teste_potencia():
    np.random.seed(0)
    A = rand_mn(3, 3)
    # v = rand_mn(3, 1)

    A = np.matrix(
        [[7.0, 2.0],
         [2.0, 4.0]])
    '''
    v = np.matrix(
        [[0.0],
         [1.0]])
    '''
    # A = A**-1
    print('A =\n', A)
    # x, l = power_iteration(A)
    # x, l = power_iteration(A, inv=True)
    D, Q = qr_iteration(A)
    # FIXME: A*Q - Q*D != 0
    # print('x =\n', x)
    # print('l =\n', l)
    # print('Erro = %e'% np.linalg.norm(A*x-l*x))

# teste_potencia()