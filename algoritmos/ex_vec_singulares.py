"""
    Exemplo para verificar os vetores singulares com SVD
"""


import numpy as np
import matplotlib.pyplot as plt

def ex_B_1_9(A: np.matrix):
    U, S, Vt = np.linalg.svd(A)
    print(U, S, Vt, sep='\n')
    V = Vt.T

    r = np.linspace(0, 2*np.pi)
    C1 = np.matrix(np.zeros((2, 50)))
    C1[0, :] = np.cos(r);   C1[1, :] = np.sin(r)
    C2 = A*C1
    xlim = min(C2[0, :].min(), C2[1, :].min()),\
           max(C2[0, :].max(), C2[1, :].max())
    ylim = xlim

    plt.figure(figsize=(10, 5))
    plt.subplot(121)
    u1 = plt.arrow(0, 0, U[0, 0], U[1, 0], color='B', head_width=0.03)
    u2 = plt.arrow(0, 0, U[0, 1], U[1, 1], color='r', head_width=0.03)
    plt.xlim(*xlim);    plt.ylim(*ylim);
    plt.grid()
    plt.legend([u1, u2], ['u1', 'u2'])
    plt.plot(np.array(C1[0, :])[0], np.array(C1[1, :])[0])

    plt.subplot(122)
    v1 = plt.arrow(0, 0, V[0, 0], V[1, 0], color='B', head_width=0.03)
    v2 = plt.arrow(0, 0, V[0, 1], V[1, 1], color='r', head_width=0.03)
    plt.xlim(*xlim);    plt.ylim(*ylim);
    plt.grid()
    plt.legend([v1, v2], ['v1', 'v2'])
    plt.plot(np.array(C2[0, :])[0], np.array(C2[1, :])[0])
    plt.tight_layout()

    plt.show()


Aa = np.matrix([[3,  0],
                [0, -2]])
Ab = np.matrix([[2, 0],
                [0, 3]])
Ac = np.matrix([[1, 1],
                [1, 1]])
Ad = np.matrix([[2, 3],
                [1, 1]])
# ex_B_1_9(Aa)
# ex_B_1_9(Ab)
# ex_B_1_9(Ac)
ex_B_1_9(Ad)