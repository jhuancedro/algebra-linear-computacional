"""
	Implementação das funcionalidades referentes a fatoração QR
	TODO: como trabalhar com matrizes triangulares em python? tem pronto?
"""

import numpy as np

zeros   = lambda m, n: np.matrix( np.zeros((m, n)) )
rand_mn = lambda m, n: np.matrix( np.random.rand(m, n) )

def gram_schmidt(A: np.matrix):
	""" Procedimento de Gram-Schimidt simples """
	m, n = A.shape
	Q = zeros(m, n)
	R = zeros(n, n)
	for j in range(n):
		vj = A[:, j]
		for i in range(j):
			R[i, j] = Q[:, i].T * A[:, j]
			vj = vj - R[i, j] * Q[:, i]
		R[j, j] = np.linalg.norm(vj)
		Q[:, j] = vj / R[j, j]

	return Q, R

def gram_schmidt_m(A: np.matrix):
	""" Procedimento de Gram-Schimidt modificado """
	m, n = A.shape
	Q = zeros(m, n)
	R = zeros(n, n)
	V = A.copy()
	for i in range(n):
		R[i, i] = np.linalg.norm(V[:, i])
		Q[:, i] = V[:, i] / R[i, i]
		for j in range(i+1, n):
			R[i, j] = Q[:, i].T * V[:, j]
			V[:, j] = V[:, j] - R[i, j] * Q[:, i]

	return Q, R

def householder(A: np.matrix):
	""" Fatoracao por householder
		<youtu.be/d-yPM-bxREs>  """
	m, n = A.shape
	Q = np.eye(m, m)    # Q pode ser desnecessario
	R = A.copy()
	for j in range(n):
		if m-j <= 1:    break

		# v = ||x||e_j - x
		v = -R[j:, j].copy()
		v[0, 0] += np.linalg.norm(v)    # *np.sign(v[0, 0])
		# v /= np.linalg.norm(v)

		H = np.eye(m-j) - 2 * v * v.T / (v.T * v)
		Q[j:,  :] = H*Q[j:,  :]     # Q pode ser desnecessario
		# R[j:, j:] = H*R[j:, j:]     # jeito classico
		R[j   , j   ] = H[0, :] * R[j:, j   ]     # elemento nao nulo da coluna
		R[j  :, j+1:] = H       * R[j:, j+1:]     # colunas nao nulas do produto matricial
		R[j+1:, j   ] = 0                         # colunas nulas

	return Q.T, R
	
def givens(A: np.matrix):
	""" Fatoracao por Givens """
	m, n = A.shape
	Q = np.eye(m, m)    # Q pode ser desnecessario
	R = A.copy()
	for k in range(n-1):
		for i in range(k+1, n):
			a1 = R[k, k]
			a2 = R[i, k]
			
			alpha = np.sqrt(a1**2 + a2**2)
			c = a1 / alpha
			s = a2 / alpha
			
			G = np.matrix(np.eye(m, m))
			G[k, k] =  c;	G[k, i] =  s;
			G[i, k] = -s;	G[i, i] =  c;
			R = G*R
			Q = G*Q

			#TODO: Evitar A multiplicação completa
			#R[k, :] =  c*R[i, :] + s*R[k, :]
			#R[i, :] = -s*R[i, :] + c*R[k, :]
			#R[k:i+1, :] = G[k:i+1, k:i+1]*R[k:i+1, :]
			#Q[k:i+1, k:i+1] = G[k:i+1, k:i+1]*Q[k:i+1, k:i+1]
			print("G=\n%s"%G)
			print("R=\n%s"%R)
			

	return Q.T, R

def teste():
	np.random.seed(0)
	A = rand_mn(3, 3)
	#A = np.matrix(
	#	[[1, 2],
	#	 [1, 0],
	#	 [1, 0]])
	print('A =\n', A)

	#Q, R = gram_schmidt(A)
	#Q, R = householder(A)
	Q, R = givens(A)
	#
	# print('Q =\n', Q)
	# print('R =\n', R)
	print('Erro na fatoração: %e' % np.linalg.norm(A-Q*R))
	
#teste()
