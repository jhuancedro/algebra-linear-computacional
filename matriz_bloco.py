import numpy as np

class MatrizBloco:
    """
        Classe para auxiliar na manipulacao de indices de matrizes em bloco
    """
    def __init__(s, M, dim):
        s.M = M;            '''Matriz completa, comum'''
        s.m = len(dim[0]);  '''Quantidade de blocos na direcao das linhas'''
        s.n = len(dim[1]);  '''Quantidade de blocos na direcao das colunas'''
        s.dim = dim;        '''Dimensao dos blocos (linhas, colunas)'''
        s.id = (np.cumsum( (0,)+dim[0] ),
                np.cumsum( (0,)+dim[1] ));  '''Auxiliar para acesso dos indices em s.M'''

    def __getitem__(s, key):
        """ Acessa o bloco referente ao indice. Retorna uma matriz comum, s[key]."""
        i, j = key
        return s.M[s.id[0][i]:s.id[0][i+1],
                   s.id[1][j]:s.id[1][j+1]]

    def __setitem__(s, key, value):
        """ Modifica o bloco referente ao indice key. s[key] = value."""
        i, j = key
        s.M[s.id[0][i]:s.id[0][i + 1],
            s.id[1][j]:s.id[1][j + 1]] = value

    def __mul__(s, B):
        """ Multiplicacao """
        return MatrizBloco(s.M*B.M, (s.dim[0], B.dim[1]))

    def __sub__(s, B):
        """ Subtracao """
        return MatrizBloco(s.M-B.M, s.dim)

    def display(s): print(s)

    def __str__(s):
        """ Visualizacao """
        t = ''
        for i in range(s.m):
            im = s.dim[0][i]
            for ii in range(im):
                if   ii == 0    and i == 0    :  t+= '+ '
                elif ii == im-1 and i == s.m-1:  t+= '+ '
                else:                            t+= '| '

                for j in range(s.n):
                    t+= s[i, j][ii, :].__str__()
                    t+= '   '
                t+= '\n'
            if i < s.m-1:   t+= '|'
            t+= '\n'

        return t