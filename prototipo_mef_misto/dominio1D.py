from itertools import product
from time import time

import pylab as pl
import sympy as sp
from scipy.integrate import quad as integral_quad

from prototipo_mef_misto.elemento1D import Elemento1D
from prototipo_mef_misto.polinomio import base_lagrange
from prototipo_mef_misto.aux_display import *


class Dominio1D:
    """Dominio de 0 a L"""
    bases = dict()
    at = False  # analise de tempo

    def __init__(s, f, L, num_elementos, H=None, grau_base=1,
                 analitica=None, dirichlet=None, neumann=None, display_n=0):
        t = time()
        s.L = L
        s.nel = num_elementos
        s.f = f
        #         s.iel = iel
        s.iel_M = None  # para assembly da matriz
        s.iel_m = None  # para assembly da solucao
        s.grau = grau_base
        s.analitica = analitica
        s.dirichlet = dirichlet
        s.neumann = neumann

        h = L / (s.nel)
        if not (s.grau, h) in Dominio1D.bases.keys():
            Dominio1D.bases[(s.grau, h)] = base_lagrange(s.grau, h)
        s.base = Dominio1D.bases[(s.grau, h)]

        s.display_v = [True if i < display_n else False for i in range(4)]

        n = s.nel
        p = s.grau
        # k dimensao da sobreposicao
        s.iel_M = tuple(tuple(i - 2 * x for i in range(x * 2 * (p + 1), (x + 1) * 2 * (p + 1))) for x in range(n))
        s.iel_m = tuple(tuple(i - 1 * x for i in range(x * (p + 1), (x + 1) * (p + 1))) for x in range(n))
        # print(s.iel_M, s.iel_m)
        if H is None:   H = [L / (s.nel)] * (s.nel)

        d = 0
        s.elementos = []
        #         print(H, s.iel)
        for h, i, j in zip(H, s.iel_M, s.iel_m):
            s.elementos.append(Elemento1D(h, d, s.base, s, i, j))
            d += h

        s.m_global = None
        s.f_global = None
        s.alphas_u = None
        s.alphas_p = None

        if Dominio1D.at: print('Classe inicializada [%f s]' % (time() - t))

    def inicializar_bases(p):
        """Cria todas as bases de grau 1 a p"""
        for i in range(1, p + 1):
            if not i in Dominio1D.bases.keys():
                Dominio1D.bases[i] = base_lagrange(i)

    def construir_matriz_global(s):
        t = time()
        m = 2 * (s.nel * s.grau + 1)
        s.m_global = sp.zeros(m, m)  # +2 condicoes de Dirichlet
        s.f_global = sp.zeros(m, 1)
        # displayEq('M', matriz)
        # display(iel, matrizes_locais)
        for e in s.elementos:
            e.construir_matriz_local(s.f, s.display_v[3])
            a, b = e.m_local, e.f_local  # a*x = b
            #             displayEq('e.iel', e.iel)
            #             displayEq('a', a)
            i_globais = list(product(e.iel_M, repeat=2))
            i_locais = list(product(range(a.shape[0]), repeat=2))
            #             display('globais', i_globais)
            #             display('locais', i_locais)
            for ig, il in zip(i_globais, i_locais):
                s.m_global[ig] += a[il]
                # display(matriz[ig],  a[il])

                # for i in range(len(e.iel)):
                #     s.f_global[e.iel[i]] += b[i]
            #             displayEq('e.iel', e.iel)
            # displayEq('b', b)
            for i in range(a.shape[0]):
                s.f_global[e.iel_M[i]] += b[i]

        if s.display_v[2]: s.display_sist_global()
        s.corrige_blocos()

        if s.display_v[2]: s.display_sist_global()

        #         s.aplicar_dirichlet(s.display_v[2])
        #         s.aplicar_neumann(s.display_v[2])

        if s.display_v[1] and not s.display_v[2]: s.display_sist_global()

        if Dominio1D.at: print('Sistema construido [%f s]' % (time() - t))

    def corrige_blocos(s):
        fg = s.f_global.copy()
        mg = s.m_global.copy()

        m, n = mg.shape
        for i, j in product(range(m), range(n)):
            mg[i, j] = s.m_global[(2 * i) % m + (2 * i) // m, (2 * j) % n + (2 * j) // n]

        for i in range(m):
            fg[i] = s.f_global[(2 * i) % m + (2 * i) // m]

        s.m_global = mg
        s.f_global = fg

    def display_sist_global(s):
        displayEq('M_global', s.m_global)
        displayEq('F_global', s.f_global)

    def solve_num(s):
        t = time()
        A = pl.array(s.m_global).astype(float)
        B = pl.array(s.f_global).astype(float)

        X = pl.linalg.solve(A, B)

        m = int(X.shape[0] / 2)
        s.alphas_u = U = X[:m]
        s.alphas_p = P = X[m:]

        if s.display_v[1]:
            displayEq('X', sp.Matrix(X))
            displayEq('U', sp.Matrix(U))
            displayEq('P', sp.Matrix(P))

        if Dominio1D.at: print('Sistema resolvido [%f s]' % (time() - t))

    def norma_L2(s):
        alphas = s.alphas_u, s.alphas_p

        def norma_aux(caso):
            norma = 0
            for e in s.elementos:
                pedaco = lambda x: sum(alphas[caso][e.iel_m[i]] * e.base[i](x - e.L0) for i in range(s.grau + 1))

                norma += integral_quad(lambda x: (pedaco(x) - s.analitica[caso](x)) ** 2, e.L0, e.L0 + e.h)[0]

            return norma ** 0.5

        return norma_aux(0), norma_aux(1)

    def plotarResultado(s):
        k = 100 // s.nel
        config = 'b-'

        def plot_aux(alphas, analitica, titulo=''):
            pl.figure(titulo)
            for ei, e in enumerate(s.elementos):
                eixo = pl.linspace(e.L0, e.L0 + e.h, k)

                pedaco = lambda x: sum(alphas[e.iel_m[i]] * e.base[i](x - e.L0) for i in range(s.grau + 1))

                pl.plot(eixo, pedaco(eixo), config, label=('Aproximação' if ei == 1 else None))
            pl.xlim(0, s.L)

            if analitica is not None:
                eixo = pl.linspace(0, s.L, 50)
                aux = [float(analitica(i)) for i in eixo]
                pl.plot(eixo, aux, 'r--', label='Analítica')

            pl.title('%s ($o = %d$, $h_e = 1/%d$)' % (titulo, s.grau, s.nel))
            pl.grid()
            pl.legend()

            pl.tight_layout()
            pl.savefig(titulo.replace('$', '') + '_' + str(s.nel) + '.eps')

        plot_aux(s.alphas_u, s.analitica[0], '$u$')
        plot_aux(s.alphas_p, s.analitica[1], '$p$')

        pl.show()

    def mef(s):
        s.construir_matriz_global()
        s.solve_num()
        if s.display_v[0]:
            s.plotarResultado()

    def imprimir(s):
        for i, e in enumerate(s.elementos):
            e.imprimir(i)
        displayEq('M_global', s.m_global)
        displayEq('F_global', s.f_global)

        # Dominio1D.inicializar_bases(3)