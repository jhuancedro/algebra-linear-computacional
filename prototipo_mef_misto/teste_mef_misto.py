import pylab as pl
import sympy as sp
from prototipo_mef_misto.aux_display import *
import numpy as np
from prototipo_mef_misto.dominio1D import Dominio1D

sp.init_printing(use_latex=True)


def teste():
    f = lambda x: pl.pi**2 * pl.sin(pl.pi * x)
    p = lambda x: pl.sin(pl.pi * x)
    u = lambda x: -pl.pi * pl.cos(pl.pi * x)

    # for n in (1, 2, 4, 8):
    omega = Dominio1D(f, analitica=(u, p), L=1.0, num_elementos=3, grau_base=1,
                      dirichlet=(None, (0, 0)), neumann=None, display_n=4)
    omega.mef()


def matriz_sistema_mef_misto(nel, grau):
    f = lambda x: pl.pi**2 * pl.sin(pl.pi * x)
    p = lambda x: pl.sin(pl.pi * x)
    u = lambda x: -pl.pi * pl.cos(pl.pi * x)

    # for n in (1, 2, 4, 8):
    omega = Dominio1D(f, analitica=(u, p), L=1.0, num_elementos=nel, grau_base=grau,
                      dirichlet=(None, (0, 0)), neumann=None, display_n=0)
    omega.construir_matriz_global()

    return pl.matrix(omega.m_global).astype(float), \
           pl.matrix(omega.f_global).astype(float)


# teste()
# print(matriz_sistema_mef_misto(10, 3))