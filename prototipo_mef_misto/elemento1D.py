from itertools import product

import sympy as sp
from scipy.integrate import quad as integral_quad
from prototipo_mef_misto.aux_display import *


class Elemento1D:
    def __init__(s, h, L0, base, dominio, iel_M, iel_m):
        s.base = base
        s.h = h
        s.L0 = L0
        s.omega = dominio
        s.m_local = None
        s.f_local = None
        s.iel_M = iel_M
        s.iel_m = iel_m

    def construir_matriz_local(s, f, display_bool=False):
        """ FIXME: Domumentacao
            Constroi  matriz local de um elemento
            [f] eh a funcao deslocada no problema -u'' = f(x)
            [grau] eh o grau dos polinimios da base"""
        m = len(s.base)
        s.m_local = sp.zeros(2 * m)
        u = p = lambda: s.base[j]
        v = q = lambda: s.base[i]
        int_omega = lambda f: f.integral_d(0, s.h)

        for i, j in product(range(m), range(m)):
            # a(u, v)
            s.m_local[2 * i, 2 * j] = (1 - 1 / 2) * int_omega(u() * v())

            # b(p, v)
            s.m_local[2 * i, 2 * j + 1] = \
                -       int_omega(p() * v().diff()) \
                - 1 / 2 * int_omega(v() * p().diff())

            # b(u, q)
            s.m_local[2 * i + 1, 2 * j] = \
                -       int_omega(q() * u().diff()) \
                - 1 / 2 * int_omega(u() * q().diff())

            # c(p, q)
            s.m_local[2 * i + 1, 2 * j + 1] += - 1 / 2 * int_omega(p().diff() * q().diff())

        # - - - - - - - - - - - - - - - - - - - - - - - - -
        s.f_local = sp.zeros(2 * m, 1)

        # f(v)
        for i in range(m):
            s.f_local[2 * i + 1] = - integral_quad(lambda x: f(x + s.L0) * q()(x), 0, s.h)[0]

        if display_bool:
            displayEq('M_local', s.m_local)
            displayEq('F_local', s.f_local)

    def imprimir(s, i):
        displayEq('Intervalo_%d' % i, (s.L0, s.L0 + s.h))
        displayEq('Bases_%d' % i, sp.Matrix([b(x) for b in s.base]))
        displayEq('Sistema\_local_%d' % i, (s.m_local, s.f_local))