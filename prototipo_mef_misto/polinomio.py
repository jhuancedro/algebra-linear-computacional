# -*- coding: utf-8 -*-

__author__ = 'jhuancedro'
__email__ = 'jhuancedro@gmail.com'
__all__ = ['Polinomio', 'base_lagrange']

import pylab as pl

class Polinomio:
    def __init__(s, *coefs):
        try:
            aux = coefs[0][0]
            s.coefs = pl.array(coefs[0]).astype(float)
        except:
            s.coefs = pl.array(coefs).astype(float)
            
        if len(coefs) == 0:
            s.coefs = [0]
            
    def __call__(s, x):
        y = 0
        for i in s.coefs[::-1]:
            y *= x
            y += i
        
        return y
    
    def diff(s, o = 1):
        novo_coefs = [i*c for i, c in enumerate(s.coefs)][1:]
        if len(novo_coefs) == 0:
            novo_coefs = (0,)
        f = Polinomio(novo_coefs)
        if o > 1:
            return f.diff(o-1)
        
        return f
    
    def integral_i(s, const=0):
        """Polinomio resultante se uma integral definida"""
        novo_coefs = [const]+[c/(i+1) for i, c in enumerate(s.coefs)]
        
        return Polinomio(novo_coefs)
    
    def integral_d(s, a, b):
        """Resultado da integral definida no intervalo [a, b]"""
        I = s.integral_i()
        
        return I(b) - I(a)
        
    def __mul__(s, b):
        if isinstance(b, Polinomio):
            p1, p2 = s.coefs, b.coefs
            p3 = pl.zeros(len(p1)+len(p2)-1)
            for i, ci in enumerate(p1):
                for j, cj in enumerate(p2):
                    p3[i+j] += ci*cj
            
            return Polinomio(p3)
        
        return Polinomio(s.coefs*b)
        
    def __add__(s, b):
        if isinstance(b, Polinomio):
            M, m = s.coefs, b.coefs
            if len(M) < len(m):
                M, m = m, M
            M = M.copy()
            M[:len(m)] += m
            
            return Polinomio(M)
        
        else:
            m = s.coefs.copy()
            m[0] += b
            
            return Polinomio(m)
        
    def __sub__(s, b):  return s.__add__(-b)
    def __radd__(s, a):  return s.__add__(a)
    def __rmul__(s, a):  return s.__mul__(a)
    def __pos__(s):  return s
    def __neg__(s):  return Polinomio(-s.coefs)
    def __truediv__(s, b):  return Polinomio(s.coefs/b)
    
    def __repr__(s):
        s1 = 'Polinomio< %s>'
        s2 = ''
        for i, c in enumerate(s.coefs):
            s2 += '%+.3f*x^%i '%(c, i)
        return s1%s2
    
    def display(s):
        display(s(sp.var('x')).expand())
    
    def plot(s, inf, sup, n=100):
        x = pl.linspace(inf, sup, n)
        pl.plot(x, s(x))
        pl.grid()
     
    
def base_lagrange(grau, h = 1):
    print('nova base [p=%d, h=%f]'%(grau, h))
    p = grau
    # pontos intermediarios de 0 a h
    nos = [i*h/p for i in range(p+1)]
    bases = []
    for j in range(p+1):
        bases.append(1)
        for k in range(p+1):
            if j != k:
                bases[j] *=  (Polinomio(0, 1)-nos[k])/(nos[j]-nos[k])
    
    return tuple(bases)
