import numpy as np

class PseudoMatriz():
    # TODO: Documentar...
    def __init__(s, C, B, solveA):
        s.C = C
        s.B = B
        s.solveA = solveA
        s.shape = C.shape

    def __mul__(s, x):
        """ Sendo S = C - B * A^-1 * B^T, retorna o produto y = S*x,
            sem montar explicitamente a matriz S """

        return s.C*x - s.B * s.solveA( (x.T * s.B).T )
