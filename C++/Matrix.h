#ifndef ALC_MATRIX_H
#define ALC_MATRIX_H

#include <iostream>

typedef unsigned int uint;
using namespace std;
template <class T> class MatrixLine;

/* Matrix de dados do tipo T */
template <class T>
class Matrix {
protected:
    T* v;
    uint m, n, mn;
public:
    Matrix(uint m, uint n) : m(m), n(n), mn(m*n) {
        v = new T[mn];
    }

    Matrix(const Matrix<T> &M) : m(M.getM()), n(M.getN()){
        mn = m*n;
        v = new T[mn];
        for (int i = 0; i < mn; ++i)
            v[i] = M.v[i];
    }

    uint getM() const {     return m;   }
    uint getN() const {     return n;   }

    T& get(uint i, uint j) const {  return v[i*n+j];    }

    void set(uint i, uint j, T x){  v[i*n+j] = x;       }

    /* Define valores aleatorios de 0 a max para todas as posicoes */
    void randM(float max){
        for (int i = 0; i < mn; ++i)
            v[i] = (T) (max * rand() / RAND_MAX);
    }

    MatrixLine<T> operator[] (uint i)  const {
        return MatrixLine<T>(this, i);
    }

    Matrix<T> operator+(const Matrix<T>& B){
        Matrix<T> S(m, n);
        for (int i = 0; i < mn; ++i) {
            S.v[i] = this->v[i] + B.v[i];
        }
        return S;
    }

    Matrix<T> operator-(const Matrix<T>& B){
        Matrix<T> S(m, n);
        for (int i = 0; i < mn; ++i) {
            S.v[i] = this->v[i] - B.v[i];
        }
        return S;
    }

    Matrix<T> operator*(const Matrix<T>& B){
        Matrix<T> C(this->m, B.getN());

        for (uint i = 0; i < this->m; ++i) {
            for (uint j = 0; j < B.getN(); ++j) {
                C[i][j] = 0;
                for (uint k = 0; k < this->n; ++k)
                    C[i][j] += (*this)[i][k] * B[k][j];
            }
        }

        return C;
    }

    Matrix<T> Tp(){
        Matrix<T> C(n, m);

        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                C.set(j, i, get(i, j));

        return C;
    }

    void imprimeLinha(uint i) const {
        cout << "[";
        for (uint j = 0; j < n - 1; ++j){
            printPos(i, j); printf(", ");
        }
        printPos(i, n - 1);
        printf("]");
    }

    virtual void imprime(const char x[]) const {
        printf("%s = \n", x);
        for (uint i = 0; i < m; ++i){
            printf("\t");
            imprimeLinha(i);
            printf("\n");
        }
        printf("\n");
    }

    void printPos(uint i, uint j) const {
        printf("% .4e", get(i, j));
    }

    ~Matrix() { delete [] v; }
};

/* Especializacao do print da posicao para matriz de inteiros */
template<>
void Matrix<int>::printPos(uint i, uint j) const {
    printf("% 3d", get(i, j));
}

/* Classe auxiliar para permitir acesso a Matriz<M> por duplo colchetes */
template <class T>
class MatrixLine{
    const Matrix<T>* M;
    const uint i;
public:
    MatrixLine(const Matrix<T>* M, uint i) : M(M), i(i) {}

    T& operator[] (uint j) const {
        return M->get(i, j);
    }

    ~MatrixLine() = default;
};

//template <class T>
//Matrix<T>& MatrixLine<Matrix<T>*>::operator[] (uint j) const {
//    return *M->get(i, j);
//}



#endif //ALC_MATRIX_H
