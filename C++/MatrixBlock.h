#ifndef ALC_MATRIXBLOCK_H
#define ALC_MATRIXBLOCK_H


//using namespace std;

#include "Matrix.h"

template <class T>
class MatrixBlock : public Matrix<Matrix<T>*>{
public:
    MatrixBlock(uint* dim[2], uint m, uint n) : MatrixBlock::Matrix(m, n) {
        for (uint i = 0; i < m; ++i) {
            for (uint j = 0; j < n; ++j) {
                this->set(i, j, new Matrix<T>( dim[0][i], dim[1][j] ));
            }
        }
    }

//    Matrix<T>& get(uint i, uint j) const override {
//        return *Matrix::get(i, j);
//    }

    void randM(float max) {
        for (int i = 0; i < this->mn; ++i) {
            this->v[i]->randM(max);
        }
    }

    void imprime(const char *x) const {
        for (int i = 0; i < this->m; ++i) {
            uint im = this->get(i, 0)->getM();
            for (int ii = 0; ii < im; ++ii) {
                printf("| ");
                for (int j = 0; j < this->n; ++j) {
                    this->get(i, j)->imprimeLinha(ii);
                    printf("   ");
                }
                printf("\n");
            }
            if(i < this->m-1)   printf("|");
            printf("\n");
        }
    }
};

#endif //ALC_MATRIXBLOCK_H
