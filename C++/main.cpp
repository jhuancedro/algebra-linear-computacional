#include <iostream>
#include "linalg.h"
#include "MatrixBlock.h"

/* Deste detalhando passos do solver por eliminacao gaussiana */
template <class T>
void papEliminacaoGauss() {
    uint m = 10, n = 10;
    Matrix<T> A(m, n), B(n, 1);

    cout << "Sistema original:" << endl;
    A.randM(10);
    B.randM(10);
    A.imprime("A");
    B.imprime("B");

    Matrix<T> A_ = A, B_ = B;

    cout << "Eliminaçao de Gauss:" << endl;
    eliminacaoGauss(A_, B_);
    A_.imprime("A_");
    B_.imprime("B_");

    cout << "Resposta do sistema:" << endl;
    Matrix<T> x = solverTriangularSup(A_, B_);
    x.imprime("X");

    // (A*x).imprime("Ax");
    (A*x-B).imprime("Ax-B");
}

template <class T>
void testeEliminacaoGauss() {
    uint m = 10, n = 10;
    Matrix<T> A(m, n), B(n, 1);//, x(m, 1);
    A.randM(10);    B.randM(10);
    //A.imprime("A"); B.imprime("B");

    Matrix<T> x = solverEliminacaoGauss(A, B);
    //x.imprime("X");

    //(A*x).imprime("Ax");
    (A*x-B).imprime("Ax-B");
}

/* Torna a matriz A triangular superior, modificando B,
 * de forma que o sistema Ax=B nao eh alterado
 * OBS:
 *  BT =  (A -1 A ) BT
 *  B  = ((Ak-1 Ai) BT)T
 * */
template <class T>
void eliminacaoGauss(MatrixBlock<T> &A, MatrixBlock<T> &B){
    /* FIXME: Sem qualquer pivotamento*/
    /* Admite-se B nx1 */
    uint m = A.getM(), n = A.getN();

    // para cada linha
    for (uint k = 0; k < n; k++) {
        //pivotamento(A, B, k);

        // cout << "Eliminacao:" << endl;
        // para cada uma das linhas abaixo
        for (uint i = k+1; i < m; i++) {
            Matrix<T> M_ik = solverEliminacaoGauss(*(A[k][k]) * (A[i][k]->Tp()), );

            // modificando todas as posicoes da linha
            //A[i][k] = 0;    // Posicao que deve ser anulada
            for (uint j = k; j < n; j++) {
//                *(A[i][j]) =  *(A[i][j]) - M_ik * (*(A[k][j]));
            }

            // modificando B
//            B[i][0] = B[i][0] - M_ik * B[k][0];
            A.imprime("A_");
            B.imprime("B_");
        }
    }
}

int main() {
    srand(1);
//    testeEliminacaoGauss<double>();
//    papEliminacaoGauss<float>();

    uint* dimA[2] = { new uint[3]{3, 2, 1}, new uint[3]{3, 2, 1} };
    uint* dimB[2] = { new uint[3]{3, 2, 1}, new uint[1]{1}    };

    MatrixBlock<float> A(dimA, 3, 3), B(dimB, 3, 1);
    A.randM(10);        B.randM(10);
    A.imprime("A");     B.imprime("B");
    eliminacaoGauss(A, B);
    A.imprime("A");     B.imprime("B");

    // desalocando...
    for (auto &p : {dimA[0], dimA[1], dimB[0], dimB[1]})
        delete [] p;

    return 0;
}