#include "Matrix.h"
#include <iostream>
#include <cmath>

/* Pivotamento para a eliminaçao gaussiana triangular inferior */
template <class T>
void pivotamento(Matrix<T> &A, Matrix<T> &B, uint k) {
    uint m = A.getN(), n = A.getN();

    // buscar maior pivo
    uint pivo = k;
    for (uint i = k+1; i < m; i++)
        if(fabs(A[i][k]) > fabs(A[pivo][k]))
            pivo = i;

    // trocando linha atual com a linha do pivo
    // nao troca valores ja nulos
    double tmp;
    for (uint j = k; j < n; j++) {
        tmp        = A[k][j];
        A[k][j]    = A[pivo][j];
        A[pivo][j] = tmp;
    }

    tmp        = B[k][0];
    B[k][0]    = B[pivo][0];
    B[pivo][0] = tmp;

    //A.imprime("A_");
    //B.imprime("B_");
}

/* Torna a matriz A triangular superior, modificando B,
 * de forma que o sistema Ax=B nao eh alterado */
template <class T>
void eliminacaoGauss(Matrix<T> &A, Matrix<T> &B){
    /* FIXME: Sem qualquer pivotamento*/
    /* FIXME: Admite-se B nx1 */
    uint m = A.getM(), n = A.getN();

    // para cada linha
    for (uint k = 0; k < n; k++) {
        //pivotamento(A, B, k);

        // cout << "Eliminacao:" << endl;
        // para cada uma das linhas abaixo
        for (uint i = k+1; i < m; i++) {
            double m_ik = A[i][k] / A[k][k];

            // modificando todas as posicoes da linha
            A[i][k] = 0;    // Posicao que deve ser anulada
            //A[i][k] = m_ik;    // Caso da decomposicao LU
            for (uint j = k+1; j < n; j++) {
                A[i][j] -= m_ik * A[k][j];
            }

            // modificando B
            B[i][0] -= m_ik * B[k][0];
        }
        //A.imprime("A_");
        //B.imprime("B_");
    }
}

template <class T>
Matrix<T> solverTriangularSup(const Matrix<T> &A, const Matrix<T> &B){
    uint m = A.getM();
    Matrix<T> x(A.getM(), B.getN());

    for (int i = m-1; i+1 > 0; --i) {
        x[i][0] = B[i][0];
        for (int j = m-1; j > i; --j)
            x[i][0] -= x[j][0]*A[i][j];
        x[i][0] /= A[i][i];
    }

    return x;
}

template <class T>
Matrix<T> solverEliminacaoGauss(const Matrix<T> &A, const Matrix<T> &B){
    Matrix<T> A_ = A, B_ = B;

    eliminacaoGauss(A_, B_);
    return solverTriangularSup(A_, B_);
}