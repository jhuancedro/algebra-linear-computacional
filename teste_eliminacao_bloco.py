from time import time

import matplotlib.pyplot as plt
import numpy as np

from algoritmos import alg_lin as al
from matriz_bloco import MatrizBloco

rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))

def testeEliminacaoGauss():
    print('Teste eliminacao gaussiana:')
    A = rand_mn(5, 5)
    B = rand_mn(5, 2)

    # print('Sistema Original', 'A =', A, 'B =', B, sep='\n')
    A_, B_, ind = al.eliminacaoGauss(A, B)
    x = al.solverTriangularSup(A_, B_, ind)

    # print('Eliminacao de Gauss', 'A_ =', A_, 'B_ =', B_, 'x', x, sep='\n')
    # print('Solucao', 'x =', x, 'Ax-B =', A*x-B, sep='\n')
    print('Erro:', np.linalg.norm(A*x-B))

def testeEliminacaoGaussBloco():
    print('Teste eliminacao gaussiana em bloco:')
    A = rand_mn(6, 6)
    B = rand_mn(6, 1)
    dimA = ((1, 3, 2), (1, 3, 2))
    dimB = ((1, 3, 2), (1,))
    A = MatrizBloco(A, dimA)
    B = MatrizBloco(B, dimB)
    # print('Sistema Original', 'A =', A, 'B =', B, sep='\n')

    A_, B_ = al.eliminacaoGaussBloco(A, B)
    # print('Eliminacao de Gauss', 'A_ =', A_, 'B_ =', B_, sep='\n')

    x = al.solverTriangularSupBloco(A_, B_)
    # print('Solucao', 'x =', x, 'Ax-B =', A*x-B, sep='\n')
    print('Erro solucao em bloco:   ', np.linalg.norm((A*x-B).M))

def testeComparacaoEliminacaoGauss(n_, n_blocos, n_testes):
    erros  = np.zeros((n_testes, 4)) # trad, bloco, trad-pivot, bloco-pivot
    tempos = np.zeros((n_testes, 4)) # trad, bloco, trad-pivot, bloco-pivot
    for i in range(n_testes):
        blocos = np.random.rand(n_blocos)
        blocos = tuple( (n_ * blocos / blocos.sum()).round().astype(int) )
        n = sum(blocos)
        print('\tTeste %3d/%3d, n=%3d, blocos=%r'%(i+1, n_testes, n, blocos))

        A = rand_mn(n, n)
        B = rand_mn(n, 1)
        dimA = (blocos, blocos)
        dimB = (blocos, (1,))
        A = MatrizBloco(A, dimA)
        B = MatrizBloco(B, dimB)

        t = time()
        x = al.solverElimicacaoGauss(A.M, B.M, pivot=False)
        tempos[i, 0] = time() - t
        erros [i, 0] = e = al.erroSolucao(A.M, B.M, x)
        # print('Erro solucao tradicional sem pivotamento:', e)

        t = time()
        x = al.solverElimicacaoGaussBloco(A, B, pivot=False)
        tempos[i, 1] = time() - t
        erros [i, 1] = e = al.erroSolucaoBloco(A, B, x)
        # print('Erro solucao em bloco sem pivotamento:   ', e)

        t = time()
        x = al.solverElimicacaoGauss(A.M, B.M, pivot=True)
        tempos[i, 2] = time() - t
        erros [i, 2] = e = al.erroSolucao(A.M, B.M, x)
        # print('Erro solucao tradicional com pivotamento:', e)

        t = time()
        x = al.solverElimicacaoGaussBloco(A, B, pivot=True)
        tempos[i, 3] = time() - t
        erros [i, 3] = e = al.erroSolucaoBloco(A, B, x)
        # print('Erro solucao em bloco com pivotamento:   ', e)

    casos = ('Tradicional', 'Bloco', 'Tradicional com\npivotamento', 'Em bloco com\npivotamento')
    titulo = ' por Eliminaçao Gaussiana (n_=%d)\n (%d blocos, %d testes)'%(n_, n_blocos, n_testes)
    plotBoxplotTestes(erros,  casos, 'Erros' +titulo, 'log')
    plotBoxplotTestes(tempos, casos, 'Tempos'+titulo)

    return erros

def plotBoxplotTestes(dados, casos, titulo, yscale='linear'):
    fig, ax = plt.subplots()
    plt.title(titulo)
    ax.boxplot(dados, labels=casos)
    ax.set_yscale(yscale)
    ax.grid()
    plt.tight_layout()
    plt.savefig('fig/' + titulo + '.png', dpi=300)
    plt.show()


np.random.seed(1)
testeEliminacaoGauss()
# testeEliminacaoGaussBloco()
# erros = testeComparacaoEliminacaoGauss(n_=100, n_blocos=5, n_testes=50)



