from itertools import product

import numpy as np
from matplotlib import pyplot as plt

from solvers.LU                 import LU_solver
from solvers.gauss_seidel       import Gauss_Seidel
from solvers.cholesky           import Cholesky_solver
from solvers.gradient_solver    import Gradient_solver, Conjugate_Gradient_solver
from algoritmos.autovalores     import power_iteration
from solvers.python_solvers     import PyCG_solver, PyGMRES_solver, PyMINRES_solver
from solvers.schur              import *

from prototipo_mef_misto.teste_mef_misto import matriz_sistema_mef_misto
from algoritmos.read_matrix.read import leitura_sistema_cesar

def gera_matriz(n):
    """ Retorna uma matriz aleatoria com um condicionamento arbitrariamente melhorado """
    A = np.matrix(np.random.rand(n, n))
    D = (n**2) * np.diag(np.random.rand(n))

    return A+D


def constroi_matriz_bloco(m, n):
    """ Constroi A matriz em bloco M, de forma que A seja SPD"""
    M_ = zerosm(m+n, m+n)
    dimM = ((m, n), (m, n))
    M = MatrizBloco(M_, dimM)

    A = gera_matriz(m);    A = A * A.T
    B = rand_mn(n, m)
    C = gera_matriz(n);    C = C * C.T

    M[0, 0], M[0, 1] = A, B.T
    M[1, 0], M[1, 1] = B,  C

    dimF = ((m, n), (1,))
    F = MatrizBloco(rand_mn(m+n, 1), dimF)

    return M, F

def constroi_matriz_bloco_mef_misto(nel, grau):
    """ Constroi a matriz em bloco a partir do problema misto de elementos finitos
        prototipado em prototipo_mef_misto """
    M_, F_ = matriz_sistema_mef_misto(nel, grau)
    m, n = M_.shape             # dimensao do sistema
    m, n = int(m/2), int(n/2)   # grau de (u) eh igual ao grau de (p)

    dimM = ((m, n), (m, n))
    M = MatrizBloco(M_, dimM)

    dimF = ((m, n), (1,))
    F = MatrizBloco(F_, dimF)

    return M, F

def constroi_matriz_bloco_cesar():
    """ Realiza a leitura de um arquivo de matriz esparsa e converte em bloco"""
    M_, F_, m, n = leitura_sistema_cesar()

    dimM = ((m, n), (m, n))
    M = MatrizBloco(M_, dimM)

    dimF = ((m, n), (1,))
    F = MatrizBloco(F_, dimF)

    return M, F


def testeUnico_SchurSystem(nel, grau):
    """ Realiza um único teste simples de resolução de um
        sistema em bloco (nel+grau)x(nel+grau) por complemento de Schur"""
    m = grau*(nel-1)
    # M, F = constroi_matriz_bloco(m, m)
    M, F = constroi_matriz_bloco_mef_misto(nel, grau)
    # M, F = constroi_matriz_bloco_cesar()


    mavl = power_iteration(M[0, 0], inv=True)[1]   # menor autovalor
    cond = np.linalg.cond( M[0, 0])                # condicionamento
    spar = (M.M == 0).sum()/M.M.size               # esparcidade

    # X, tempo = solveSchurSystem(M, F, solver=Cholesky_solver(), solverSPD=Gradient_solver())
    X, tempo = solveBlockSystem(M, F, ClassSolverA=Cholesky_solver, ClassSolverSchur=LU_solver)
    erro = np.linalg.norm( (M * X - F).M )    # norma do residuo
    print("\nMenor Autovalor (A):", mavl)
    print("Cond (A):", cond)
    print("Esparcidade:", spar)
    print("Erro: ", erro)


def testesSchurSystem(nel, grau, n_testes):
    """ Realiza varios teste de resolução de sistemas em bloco
        (nel+grau)x(nel+grau) por complemento de Schur.
        O sistema é gerado com numeros aleatorios de 0 A 1.
        A matriz A (superior esquerda do sistema em bloco) tem seu
        menor autovalor restrito A 0.01.

        :returns: Retorna dos testes os tempos e normas dos residuos,
        condicionamentos de A, menores autovalores de A, e o nome dos casos realizado
    """
    # Resolvedores para sistema com a matriz A
    a_solvers     = [LU_solver, Cholesky_solver, Conjugate_Gradient_solver]
    # Resolvedores para  sistema de Schur
    schur_solvers = [Conjugate_Gradient_solver, PyCG_solver, PyMINRES_solver, PyGMRES_solver]
    # Resolvedores para sistema completo (sem tecnicas de resolucao em bloco)
    geral_solvers = [LU_solver, Conjugate_Gradient_solver]

    casos  = tuple(product(schur_solvers, a_solvers))

    num_casos = len(geral_solvers)+len(casos)
    tempos   = np.zeros(num_casos)   # Tempo de resolucao do sistema em bloco
    itsSchur = np.zeros(num_casos)   # Numero de iteracoes da resolucao do sistema de Schur
    resSchur = np.zeros(num_casos)   # Norma do residuo da resolucao do sistema de Schur
    resBlock = np.zeros(num_casos)   # Norma do residuo da resolucao do sistema em bloco
    condM   = None
    condA   = None

    nomes_casos = [ gs.name for gs in geral_solvers]+\
                  [ (s1.name + ' + ' + s2.name) for s1, s2 in casos]

    M, F = constroi_matriz_bloco_mef_misto(nel, grau)
    S = M[1, 1] - M[1, 0] * M[0, 0].I * M[0, 1]     # apenas para avaliacao dos testes
    avalM  = min(np.linalg.eigvals(M.M))            # menor autovalor de M
    avalS  = min(np.linalg.eigvals(S))              # menor autovalor de S
    avalA  = min(np.linalg.eigvals(M[0, 0]))        # menor autovalor de A
    condM =  np.linalg.cond(M.M)                    # condicionamento da matriz em bloco
    condS =  np.linalg.cond(S)                      # condicionamento do complemento de Schur
    condA =  np.linalg.cond(M[0, 0])                # condicionamento da matriz A
    sparM = (M.M == 0).sum()/M.M.size                # esparcidade da matriz M
    print("\nMenor Autovalor (M):", avalM)
    print("Menor Autovalor (S):", avalS)
    print("Menor Autovalor (A):", avalA)
    print("Condicionamento (M):", condM)
    print("Condicionamento (A):", condA)
    print("Esparcidade:", sparM)

    # Para cada metodo em que se resolve o sistema completo sem divisao em blocos (tradicional)
    for c, solverClass in enumerate(geral_solvers):
        print("\nCASO %2d (%s):"%(c, nomes_casos[c]))
        solver = solverClass()
        t = time()
        if isinstance(solver, (LU_solver, Cholesky_solver)):
            X = solver.solve(M.M, F.M)
            resBlock[c] = np.linalg.norm( M.M * X - F.M )
        else:
            X, itsSchur[c], resBlock[c] = solver.solve(M.M, F.M)
        tempos[c] = t
        print(" Iteracoes: %3d,\t Residuo: %.10e" % (itsSchur[c], resBlock[c]), end=' ')
        print("erro: %.10e"% resBlock[c])

    # Para cada metodo em que se resolve o sistema por blocos
    for c, n_s in enumerate(casos, len(geral_solvers)):
        print("\nCASO %2d (%s):"%(c, nomes_casos[c]))
        X, itsSchur[c], resSchur[c], tempos[c] = solveBlockSystem(M, F, ClassSolverA=n_s[1], ClassSolverSchur=n_s[0])
        resBlock[c] = np.linalg.norm( (M * X - F).M )    # norma do residuo

        print("erro: %.10e, Iteracoes: %3d,\t Residuo: %.10e" % (resBlock[c], itsSchur[c], resSchur[c]))

    return tempos, itsSchur, resSchur, resBlock, sparM, condM, condS, condA, avalM, avalS, avalA, nomes_casos


def boxplot(serie, titulo, labels=None, save=False):
    """ Desenha e salva boxplot """
    plt.figure()
    plt.bar(serie, labels=labels)
    plt.title(titulo)
    plt.grid()
    if save: plt.savefig('fig/'+titulo+'.png', dpi=200)


def plotResultados(resultados, pref=''):
    """ Cria boxplots com os resultados de cada metodo """
    tempos, erros, conds, aval, nomes_casos = resultados
    boxplot(erros, pref+'Norma do resíduo', nomes_casos,   save=True)
    boxplot(tempos, pref+'Tempos de execução', nomes_casos, save=True)
    # boxplot(conds,  pref+'Condicionamento da matriz A',     save=True)
    # boxplot(aval,   pref+'Menor autovalor da matriz A',     save=True)
    # plt.show()


def write_output_testes(testes, resultados, labels):
    """ Recreve resultados em um arquivo .csv"""
    linhas_problema = \
        [ "Caso de teste",                          # [ 0]
          "Número de elementos do problema",        # [ 1]
          "Ordem da solução",                       # [ 2]
          "Dimensão da matriz M",                   # [ 3]
          "Esparcidade da matriz M",                # [ 4]
          "Condicionamento da matriz M",            # [ 5]
          "Condicionamento da matriz S",            # [ 6]
          "Condicionamento da matriz A",            # [ 7]
          "Menor autovalor da matriz M",            # [ 8]
          "Menor autovalor da matriz S",            # [ 9]
          "Menor autovalor da matriz A",            # [10]
          ""                                        # [11]
         ]

    linhas_problema[11] += "\nMetodo" \
                           ";Tempo (s)" \
                           ";Iteraçoes no Sistema de Schur" \
                           ";Residuo no sistema de Schur" \
                           ";Residuo do sistema em bloco"

    for teste, resultado, label in zip(testes, resultados, labels):
        linhas_problema_i = linhas_problema.copy()

        t, iS, rS, rB, spM, cdM, cdS, cdA, avM, avS, avA, nc = resultado
        linhas_problema_i[ 1] += ";%d"%teste[0]       # numero de elementos
        linhas_problema_i[ 2] += ";%d"%teste[1]       # Ordem da solucao
        linhas_problema_i[ 3] += ";%d"%( 2*teste[0]*teste[1] + 2 )  # Dimensao da matriz em blocos
        linhas_problema_i[ 4] += ";%.5f%%"%(100*spM)  # esparcidade da matriz em blocos
        linhas_problema_i[ 5] += ";%E"%cdM            # condicionamento da matriz em blocos
        linhas_problema_i[ 6] += ";%E"%cdS            # condicionamento da matriz em blocos
        linhas_problema_i[ 7] += ";%E"%cdA            # condicionamento da matriz A
        linhas_problema_i[ 8] += ";%E"%avM            # menor autovalor da matriz A
        linhas_problema_i[ 9] += ";%E"%avS            # menor autovalor da matriz A
        linhas_problema_i[10] += ";%E"%avA            # menor autovalor da matriz A

        linhas_resultados_i = ["%s;%E;%d;%E;%E"%(nome_caso, t[i], iS[i], rS[i], rB[i]) for i, nome_caso in enumerate(nc)]

        arq = open('out/resultados_%s.csv'%label, 'w')
        arq.writelines("%s\n"%l for l in linhas_problema_i)
        arq.writelines("%s\n"%l for l in linhas_resultados_i)
        arq.close()


# np.random.seed(0)
def serie_testes():
    testes = ((10, 1), (100, 1), (100, 2))#, (150, 3))
    resultados = []
    labels = []
    for m, n in testes:
        print('\n\n############# TESTES %3delem+grau%1d (m=n=%d) #############'%(m, n, 2*(m*n+1)))
        resultados.append(testesSchurSystem(m, n, 10))
        # plotResultados(resultados, pref='[%03delem+grau%03d]'%(m, n))
        labels.append('[%03delem+grau%03d]' % (m, n))
    write_output_testes(testes, resultados, labels)



serie_testes()
# teste_SchurSystem(10, 2)
