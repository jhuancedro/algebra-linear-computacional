"""
	Métodos de Gradiente Conjugado, MINRES e GMRES implementados pelo python
"""

import numpy as np
from solvers.solver import Solver
from scipy.sparse.linalg import cg, minres, gmres

zeros = lambda m, n: np.matrix(np.zeros((m, n)))

class Callback_counter:
    def __init__(s):
        s.n_it = 0

    def __call__(s, rk=None):
        s.n_it += 1

class PyIterative_solver(Solver):
    def __init__(s, solver):
        s.solver = solver

    def solve(s, A: np.matrix, B: np.matrix):
        contador = Callback_counter()
        X = zeros(*B.shape)
        for k in range(B.shape[1]):
            X[:, k] = np.matrix( s.solver(A, B[:, k], tol=1e-10, callback=contador)[0] ).T

        res = np.linalg.norm( A * X - B )

        return X, contador.n_it, res


class PyCG_solver(PyIterative_solver):
    name = "Gradiente Conjugado (py)"
    def __init__(s):
        super().__init__(cg)


class PyMINRES_solver(PyIterative_solver):
    name = "MINRES (py)"
    def __init__(s):
        super().__init__(minres)


class PyGMRES_solver(PyIterative_solver):
    name = "GMRES (py)"
    def __init__(s):
        super().__init__(gmres)
