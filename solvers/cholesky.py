import numpy as np

from solvers.solver import Decomposition_Solver

rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))


class Cholesky_solver(Decomposition_Solver):
    name = "Cholesky"
    def decomposition(s, A):
        """ Decomposição de Cholesky, onde A = G*G^T    """

        G = np.matrix(np.zeros(A.shape))

        for i in range(G.shape[0]):
            for j in range(i + 1):
                G[i, j] = A[i, j] - sum(G[i, k] * G[j, k] for k in range(j))
                # G[i, j] = A[i, j] - (G[i, :j] * G[j, :j].T)
                if i == j:
                    G[i, i] = abs(G[i, i]) ** (1 / 2)
                else:
                    G[i, j] = G[i, j] / G[j, j]

        s.D = G

    def solve_decomposition(s, B):
        G = s.D
        n = G.shape[0]
        y = np.matrix(np.zeros(B.shape))
        x = np.matrix(np.zeros_like(y))

        # Resolve Gy = B
        for i in range(n):
            y[i, :] = B[i, :]

            if i > 0:
                y[i, :] -= G[i, :i] * y[:i, :]

            y[i, :] /= G[i, i]

        # Resolve G^T x = y
        for i in range(n - 1, -1, -1):
            x[i, :] = y[i, :]

            if i < n - 1:
                x[i, :] -= G.T[i, i + 1:] * x[i + 1:, :]

            x[i, :] /= G[i, i]

        return x
