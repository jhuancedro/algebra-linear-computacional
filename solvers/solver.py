import numpy as np

class Solver:
    name = 'Solver'
    def solve(s,  A: np.matrix, B: np.matrix):
        return None

class Decomposition_Solver(Solver):
    def __init__(s):
        s.D = None  # decomposition

    def decomposition(s, A: np.matrix):
        return None

    def solve_decomposition(s, B: np.matrix):
        return None

    def solve(s, A: np.matrix, B: np.matrix):
        s.decomposition(A)
        return s.solve_decomposition(B)
