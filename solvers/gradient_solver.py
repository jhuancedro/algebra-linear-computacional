"""
	Métodos de Gradiente e Gradiente Conjugado
"""

import numpy as np
from pseudo_matrix import PseudoMatriz
from solvers.solver import Solver

zerosm = lambda m, n: np.matrix(np.zeros((m, n)))

class Gradient_solver(Solver):
    name = "Gradiente"
    def solve(s, A: PseudoMatriz, B: np.matrix, max_it=None, eps = None):
        """Método do Gradiente
            max_it: numero maximo de iteracoes
            eps:    norma estimada do residua
        Obs.: resolve para qualquer quantidade de colunas de B  """

        if (eps is None) and (max_it is None):
            eps = 1e-10
            max_it = 1e4

        stop = lambda: ( (max_it is not None) and (it >= max_it) ) or \
                       ( (eps    is not None) and (np.linalg.norm(r) <= max(eps, eps*nr0)) )

        X = np.matrix(np.random.rand(*B.shape))
        it=0
        for k in range(B.shape[1]):
            x, b = X[:, k], B[:, k]
            r = b - A * x
            nr0 = np.linalg.norm(r)
            while not stop():
                a = float(r.T * r / (r.T * A * r))
                x = x + a * r
                r = b - A * x
                it+=1

            X[:, k] = x

        return X, it, np.linalg.norm(r)

class Conjugate_Gradient_solver(Solver):
    name = "Gradiente Conjugado"
    def solve(s, A: PseudoMatriz, B: np.matrix, max_it=None, eps = None):
        """Método do Gradiente Conjugado
            max_it: numero maximo de iteracoes
            eps:    norma estimada do residua
        Obs.: resolve para qualquer quantidade de colunas de B  """

        if (eps is None) and (max_it is None):
            eps = 1e-10
            max_it = 1e4

        stop = lambda: ( (max_it is not None) and (it >= max_it) ) or \
                       ( (eps    is not None) and (np.linalg.norm(r) <= max(eps, eps*nr0)) )

        rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))

        X = zerosm(*B.shape)                # solucao inicial
        # X = np.matrix(np.random.rand(*B.shape))                # solucao inicial
        it=0
        for k in range(B.shape[1]):
            m, n = A.shape
            x, b = X[:, k], B[:, k]
            d = r = b - A * x          # direcao e residuo iniciais
            nr0 = np.linalg.norm(r)         # norma dos residuos inicial
            s1 = float(r.T * r)             # norma do residuo atual
            while not stop():
                q = A*d
                a = s1/float(d.T*q)         # passo da solucao
                x = x + a*d                 # atualiza solucao
                if it%m:   r = b - A * x    # atualiza residuo
                else:      r = r - a*q      # estima residuo
                s0, s1 = s1, float(r.T*r)   # atualiza norma dos residuos anterior e atual
                beta = s1 / s0
                d = r + beta*d              # atualiza direcao da busca
                it+=1
                # print('\t\tit=%3d, res=%.5e' % (it, np.linalg.norm(r)))

            X[:, k] = x

        return X, it, np.linalg.norm(r)

def teste():
    A = np.matrix([[3, 2],
                   [2, 6]])

    b = np.matrix([2, -8]).T

    x = grad(A, b, 3)
    x = gradc(A, b)
    # print(x)

#teste()