from time import time
import numpy as np

from solvers.LU       import LU_solver
from solvers.cholesky import Cholesky_solver

from matriz_bloco  import MatrizBloco
from pseudo_matrix import PseudoMatriz
from solvers.gradient_solver import Gradient_solver, Conjugate_Gradient_solver

rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))
zerosm  = lambda m, n: np.matrix(np.zeros((m, n)))

def solveSystemSchur(solverA, solverSchur, B, C, fs):
    if isinstance(solverSchur, (Gradient_solver, Conjugate_Gradient_solver)):
        # Caso o reolvedor do sistema seja o Gradiente ou o gradiente conjugado
        # implementados em solver, usar a PseudoMatriz para nao construir a matriz S
        S = PseudoMatriz(C, B, solverA)  # S  = C  - B*(A^-1)*(B.T)  [1]
    else:
        # para os demais resolvedores, a matriz S eh construida normalmente
        S = C - B * solverA(B.T)

    p, it, res = solverSchur.solve(S, fs)  # p = (S^-1)*fs

    # print('\tit=%3d, res=%.5e' % (it, res))

    return p, it, res


def solveBlockSystem(M: MatrizBloco, F: MatrizBloco, ClassSolverA, ClassSolverSchur):
    """ Resolve o sistema em bloco M*X = F por Complemento de Schur
        Recebe como parametro um resolvedor de sistema linear comum e outro
        resolvedor para matrizes SPD.

        :returns: Retorna o resultado e o tempo gasto na resolucao. """

    # renomeia os blocos do sistema
    A, B, C = M[0, 0], M[1, 0], M[1, 1]
    f1, f2  = F[0, 0], F[1, 0]


    solverSchur = None  # metodo para resolver o sistema se Schur
    solverA     = None  # metodo para resolver sistemas com matriz A

    solverSchur = ClassSolverSchur()  # metodo para resolver o sistema se Schur

    tempo = time()

    # Caso o resolvedor da matriz A decomponha a matriz
    if issubclass(ClassSolverA, (LU_solver, Cholesky_solver)):
        instanceSolverA = ClassSolverA()

        # instanceSolverA decompoe a matriz A (em LU ou LL^T, a depender do metodo)
        instanceSolverA.decomposition(A)

        # instanceSolverA.solve_decomposition(B) utiliza a decomposicao de A para resolver AX = B
        solverA = instanceSolverA.solve_decomposition
    else:
        # Caso o resolvedor não decomponha a matriz
        instanceSolverA = ClassSolverA()
        # solverA = lambda B: instanceSolverA.solve(A, B)[0]
        def solverA(B):
            x, it, res = instanceSolverA.solve(A, B)
            # print('\tit=%3d, res=%.5e'%(it, res))
            return x


    # print('fs = f2 - B*(A^-1)*f1')
    fs = f2 - B * solverA(f1)   # fs = f2 - B*(A^-1)*f1     [2]

    # print('p = (S^-1)*(C  - B*(A^-1)*(B.T))')
    p, it, res = solveSystemSchur(solverA, solverSchur, B, C, fs)

    # print('u = (A^-1)*(f1-(B.T)*p)')
    u = solverA(f1 - B.T * p)    # u = (A^-1)*(f1-(B.T)*p)   [3]

    tempo  = time() - tempo

    X = MatrizBloco(zerosm(*F.M.shape), F.dim)
    X[0, 0], X[1, 0] = u, p

    return X, it, res, tempo
