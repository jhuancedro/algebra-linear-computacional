import numpy as np
from solvers.solver import Solver

rand_mn = lambda m, n: np.matrix(np.random.rand(m, n))

class Gauss_Seidel(Solver):
    name = 'Gauss Seidel'
    def solve(s, A: np.matrix, B: np.matrix, x0 = None, max_it = None, tol_erro = None):
        if x0 is None:   X = np.zeros_like(B)

        if (tol_erro is None) and (max_it is None):
            tol_erro = 1e-10
            max_it   = 1e3

        stop = lambda: ( (max_it   is not None) and (k >= max_it) ) or \
                       ( (tol_erro is not None) and (abs(erro.sum() - x.sum())) < tol_erro )

        for j in range(B.shape[1]):
            b = B[:, j]
            x = X[:, j]
            erro = np.ones_like(x)*np.inf

            # Gauss Seidel de fato:
            k = 0
            while not stop():
                for i in range(A.shape[0]):
                    soma = A[i, :] * x - A[i, i]*x[i]
                    erro = x.copy()
                    x[i] = (b[i] - soma) / A[i, i]
                k += 1

        return X

def teste_gauss_seidel():
    dim = 200
    A = rand_mn(dim, dim)
    B = rand_mn(dim, 3)

    for i in range(dim):
        A[i, i] += 10

    x = Gauss_Seidel().solve(A, B)
    print(np.linalg.norm(A * x - B))

# teste_gauss_seidel()