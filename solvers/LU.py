import numpy as np

from solvers.solver import Decomposition_Solver

zerosm = lambda m, n: np.matrix(np.zeros((m, n)))

class LU_solver(Decomposition_Solver):
    name = "LU"
    def decomposition(s, A: np.matrix):
        """ Decompoe A em LU
            Nao modifica A matriz original. """

        lu = A.copy()
        m, n = lu.shape

        # para cada linha
        for k in range(n):
            # para cada uma das linhas abaixo
            for i in range(k+1, m):
                m_ik = lu[i, k] / lu[k, k]
                # modificando todas as posicoes da linha
                lu[i, k] = m_ik # Caso da decomposicao LU
                lu[i, k+1:n] -= m_ik * lu[k, k+1:n]

        s.D = lu    # decomposicao

    def solverU(s, y: np.matrix):
        """ Resolve o sistema Ux = y,
            onde U eh A triangular superior de LU com 1 na diagonal """
        LU = s.D
        m, n = LU.shape[0], y.shape[1]
        x = zerosm(m, n)

        for i in range(m - 1, -1, -1):
            x[i, :] = y[i, :]
            if i < m - 1:
                x[i, :] -= LU[i, i + 1:] * x[i + 1:, :]
            x[i, :] /= LU[i, i]

        return x

    def solverL(s, B: np.matrix):
        """ Resolve o sistema Ly = B,
        onde L eh A triangular inferior de LU com 1 na diagonal """
        LU = s.D
        m, n = LU.shape[0], B.shape[1]
        y = zerosm(m, n)

        for i in range(m):
            y[i, :] = B[i, :]
            if i > 0:
                y[i, :] -= LU[i, :i] * y[:i, :]
                # diagonal de L eh sempre 1 (nao presica dividir por L_ii)

        return y

    def solve_decomposition(s, B: np.matrix):
        """ Para A decomposicao LU (em uma unica matriz), resolve o sistema LUx=B """
        y = s.solverL(B)
        x = s.solverU(y)

        return x